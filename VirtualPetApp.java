import java.util.Scanner;
public class VirtualPetApp {
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		Bear[] sloth = new Bear[1];
		
		for(int i = 0; i < sloth.length; i++){
			System.out.println("Enter the food for the bear:");
			String food = reader.nextLine();
			
			System.out.println("Enter the height of the bear:");
			int height = Integer.parseInt(reader.nextLine());

			System.out.println("Enter the colour of the bear:");
			String colour = reader.nextLine();

			sloth[i] = new Bear(colour, height, food);
		}
		
		// updating food field for last animal in array
		System.out.println("Enter another food for the bear:");
		String food = reader.nextLine();
		sloth[sloth.length - 1].setFood(food);
		
		// printing all fields
		System.out.println(sloth[0].getFood());
		System.out.println(sloth[0].getHeight());
		System.out.println(sloth[0].getColour());
		
	}
}
	