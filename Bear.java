public class Bear {
	private String colour;
	private int height;
	private String food;
	
	public Bear(String colour, int height, String food){
			this.colour = colour;
			this.height = height;
			this.food = food;
	}
		
	// set methods
	public void setFood(String food){
		this.food = food;
	}
	
	// get methods
	public String getColour(){
		return this.colour;
	}
	public int getHeight(){
		return this.height;
	}
	public String getFood(){
		return this.food;
	}
	
	// This method takes the field food and if it's equals to food and if the field height is greater
	// than or equal to 50 then return the String that bear is hungry if the height is less than 50 then
	//return too young
	public String huntFood(){
		if(this.food.equals("goat") && this.height >= 50){
			return "Bear is hungry and should hunt food";
		}
		else if(this.food.equals("goat") && this.height < 50){
			return "Bear is hungry, but still too young to hunt";
		}
		else {
			return "Bear is full";
		}
	}
	
	//This method takes the field colour and if it's equal to white and food field is vodka then return a string
	// and if the colour is brown then return that the bear is lazy and else it return the black bear
	public String dancingBear(){
		if(this.colour.equals("white") && this.food.equals("vodka")){
			return "Dance because bear is drunk";
		}
		else if(this.colour.equals("brown") && this.food.equals("vodka")){
			return "Dancing while dreaming and too lazy to wake up";
		}
		else {
			return "Black bear is always dancing no matter what";
		}
	}
}
	